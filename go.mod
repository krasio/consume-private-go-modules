module github.com/christopherbiscardi/my-go-project

go 1.13

require (
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	gitlab.com/krasio/color v1.7.0
)
